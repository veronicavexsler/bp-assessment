# BP Pulse Electrification (EV) Coding Exercise: ChargePoint Widget 
___
**Exercise: time spent on this should be no more than 2-3 hours**

Using React Native, Typescript, GraphQL


## The business need:
A new ChargePoint widget is needed across several Electric Vehicle (EV) mobile consumer apps. The business wants to add this new widget for several live EV mobile apps, with a plan to migrate and merge into a single global app.

To reduce the time, effort and avoid duplicated code is to build this widget as a reusable component which can be published and imported into any Electric Vehicle (EV) mobile app. 


## The Requirement:
For the purpose of this coding exercise:

1. Create a new react native app which will host this new ChargePoint Widget
2. Create the new react native ChargePoint Widget 
3. Use the provided GraphQL server code sample, have it run locally. It contains a single query returning the json test data
4. The ChargePoint Widget must consume data from a GraphQL client 
5. List the top 3 closest ChargePoints ordered by distance 
6. On ‘Show all’ expand list to max of 10 ChargePoints ordered by distance 

7. On ‘Show less’ reduce list size to 3 ChargePoints ordered by distance 


ChargePoint Widget to develop:
(Please use similar images and colours where possible – no assets are provided)

| Picture 1 | Picture 2 |
|--|--|
| <img src="images/Picture 1.png"> |  <img src="images/Picture 2.png">|

### #Note: Please ensure you demonstrate best react native coding practices and include tests.

## Exercise Submission
Once completed please zip your code exercise and send to primary jagdeep.virdi@bp.com, chay.carnell1@bp.com  and ross.johnson1@bp.com 
If successful, you will be invited to a virtual code interview. During the code interview be prepared to walk through your solution and have a demo ready. Also be ready to discuss on further feature improvements and mobile app enhancements.

