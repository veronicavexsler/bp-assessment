import ChargePointDataMockJson from '../datasource/chargepointData.json'

const chargePointResolver = {
  Query: {
    getAllChargePoints:  () => ChargePointDataMockJson,
  },
};

exports.resolver = chargePointResolver;
