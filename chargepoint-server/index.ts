import express from "express";
import { createServer } from "http";

import initializeApolloServer from "./apolloServer";

const app = express();
const server = createServer(app);

// Initialize Apollo server
initializeApolloServer(app);

server.listen({ port: 4010 }, () => {
  console.log("ChargePoint Server Ready on port 4010");
});

// Shut down in the case of interrupt and termination signals
["SIGINT", "SIGTERM"].forEach((signal) => {
  process.on(signal, () => server.close());
});
