import { ApolloServer, gql } from "apollo-server-express";
import {
  ApolloServerPluginInlineTraceDisabled,
  ApolloServerPluginLandingPageGraphQLPlayground,
} from "apollo-server-core";
import { buildSubgraphSchema } from "@apollo/federation";
import glue from "schemaglue";

// Glue schemas/resolvers together
const { schema, resolver } = glue("src/graphql", { mode: "ts" });

// Initialize Apollo server
const initializeApolloServer = (app: any) => {
  const server = new ApolloServer({
    schema: buildSubgraphSchema({
      typeDefs: gql(schema),
      resolvers: resolver,
    }),
    plugins: [
      ApolloServerPluginInlineTraceDisabled(),
      ApolloServerPluginLandingPageGraphQLPlayground(),
    ],
  });
  // Middleware: GraphQL
  server.start().then(() =>
    server.applyMiddleware({
      app,
    })
  );
};

export default initializeApolloServer;
