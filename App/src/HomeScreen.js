import { useQuery, gql } from '@apollo/client';
import { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, View, FlatList, Text, Pressable, Image } from 'react-native';

let displayAllChargers = false;

export const CHARGE_POINT_QUERY = gql`
query ChargePoint {
  getAllChargePoints {
    stationName
    location
    availableChargePoints
    totalChargePoints
    isFavourite
    distance
  }
}`

export function DisplayChargePoints() {
  const { loading, error, data } = useQuery(CHARGE_POINT_QUERY);
  const [buttonTitle, setButtonTitle] = useState('Show Less');

  useEffect(() => {
   setButtonTitle(displayAllChargers ? 'Show Less' : 'Show More')
  })
  
  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text>{'Error: ' + error.message}</Text>;

  let filteredChargers = [];
  if(!displayAllChargers) {
    filteredChargers = data?.getAllChargePoints.slice(0,3)
  } else {
    filteredChargers = data?.getAllChargePoints
  }

  return <SafeAreaView style={styles.chargePoints} >
  <FlatList 
    style={styles.flatList}
    data={filteredChargers}
    renderItem={chargePoint}
    keyExtractor={data => data.stationName}
    />    
    <Pressable style={styles.button} onPress={() => { displayAllChargers = !displayAllChargers ; setButtonTitle()}}>
      <Text style={styles.text}>{buttonTitle}</Text>
    </Pressable> 
  </SafeAreaView>
}

const ChargePointPressable = ({ availableChargePoints, distance, isFavourite, location, stationName, totalChargePoints }) => (
  <Pressable style={styles.pressableContainer}>
    <View style={styles.viewContainer}>
      <View style={[styles.firstColumn, {flexDirection:1}]}>
        <Text style={[styles.h2, {marginBottom:5}]}>{availableChargePoints}/{totalChargePoints}</Text>
        <Text>Available</Text>
      </View>
      <View style={[styles.secondColumn, {flexDirection:2}]}>
        <Text style={styles.h5}>{stationName}</Text>
        <Text style={styles.locationText}>{location}</Text>
      </View>
      <View style={[styles.thirdColumn, {flexDirection:3}]}>
        <Image
          style={styles.favoriteIcon}
          source={isFavourite ? require('../assets/star-solid.svg') : ''}
        />
      </View>
      <View style={styles.fourthColumn}>
        <Image
          style={styles.navigationIcon}
          source={require('../assets/diamond-turn-right-solid.svg')}
        />
        <Text style={styles.distanceStyle}>{distance} m</Text>
      </View>
    </View>
  </Pressable>
);

const chargePoint = ({ item }) => (
  <ChargePointPressable availableChargePoints={item.availableChargePoints} distance={item.distance} isFavourite={item.isFavourite} location={item.location} stationName={item.stationName} totalChargePoints={item.totalChargePoints}/>
);

export default function HomeScreen() {
  return (
    <View>
      <Text style={styles.h4}>Hello</Text>
      <Text style={[styles.h2, styles.subTitle]}>USER NAME</Text>
      <Text style={styles.h4}>Nearby Chargers</Text>
      <Text style={styles.h5}>Nearest charging stations within a radius of 50km</Text>
      <Text style={styles.h5}>Your preference: CCS Type 2</Text>
      <DisplayChargePoints />
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    border: 2 + 'px solid blue',
    borderRadius: 30,
    elevation: 3,
    backgroundColor: 'white',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'black',
  },
  h2: {
    fontSize: 30,
    marginLeft: 0,
    marginRight: 0,
    fontWeight: 'bold',
    marginTop:0,
    marginBottom: 10
  },
  h4: {
    marginLeft: 0,
    marginRight: 0,
    fontWeight: 'bold',
    marginBottom: 10,
    fontSize: 25
  },
  h5: {
    marginLeft: 0,
    marginRight: 0,
    fontWeight: 'light',
    fontSize: 20,
    color: 'grey'
  },
  favoriteIcon: {
    width: 20, 
    height: 20
  },
  navigationIcon: {
    width: 40, 
    height: 40,
    justifyContent: 'center',
    alignContent: 'center'
  },
  firstColumn: {
    width: 25 + '%',
    paddingLeft: 15
  },
  secondColumn: {
    width: 45 + '%'
  },
  thirdColumn: {
    width: 10 + '%'
  },
  fourthColumn: {
    backgroundColor: '#EDE8E8',
    width: 20 + '%',
    alignItems:'center',
    flexDirection: 4, 
    height: 90,
    paddingTop: 20
  },
  distanceStyle: {
    textAlign: 'center'
  },
  subTitle: {
    marginBottom: 60
  },
  chargePoints: {
    paddingTop: 15
  },
  flatList: {
    marginBottom: 20,
    borderRadius: 15, 
    border: 2 + 'px solid lightGrey'
  },
  locationText: {
    marginTop: 16
  },
  pressableContainer: {
    borderBottomColor: '#d5d0d0',
    borderBottomWidth: 2
  },
  viewContainer: {
    flexDirection:"row", 
    alignItems: 'center', 
    height: 90
  }
});