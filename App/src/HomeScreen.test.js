import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { MockedProvider } from "@apollo/client/testing";
import { CHARGE_POINT_QUERY, DisplayChargePoints} from './HomeScreen'

const mocks = [
  {
    request: {
      query: CHARGE_POINT_QUERY,
    },
    result: {
      data: {
        getAllChargePoints: [
          {
            stationName: 'Paddington 36',
            location: 'W2 5TY, UK',
            availableChargePoints: '9',
            totalChargePoints: '18',
            isFavourite: true,
            distance: '140',
          },
          {
            stationName: 'London 2',
            location: 'W2 5TY, UK',
            availableChargePoints: '3',
            totalChargePoints: '10',
            isFavourite: false,
            distance: '12000',
          },
          {
            stationName: 'London 4',
            location: 'W2 5TY, UK',
            availableChargePoints: '4',
            totalChargePoints: '10',
            isFavourite: false,
            distance: '12000',
          },
          {
            stationName: 'London 5',
            location: 'W2 5TY, UK',
            availableChargePoints: '5',
            totalChargePoints: '10',
            isFavourite: false,
            distance: '12000',
          },]
      }
    }
  }
];

it("renders without error", async () => {
  render(
    <MockedProvider mocks={[]} addTypename={false}>
        <DisplayChargePoints/>
    </MockedProvider>
  );
  expect(await screen.findByText("Loading...")).toBeInTheDocument();
});

it('renders charge points', async ()  => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
           <DisplayChargePoints />
        </MockedProvider>
      );

      expect(await screen.findByText("Loading...")).toBeInTheDocument();
      expect(await screen.findByText("Paddington 36")).toBeInTheDocument();
      expect(await screen.findByText("Show More")).toBeInTheDocument();
})

it("should show error UI", async () => {
    const errorMock = [
  {
    request: {
      query: CHARGE_POINT_QUERY,
    },
    error: new Error('An error occurred')
  }
];
    render(
      <MockedProvider mocks={errorMock} addTypename={false}>
        <DisplayChargePoints />
      </MockedProvider>
    );
    
    expect(await screen.findByText("Error: An error occurred")).toBeInTheDocument();
  });