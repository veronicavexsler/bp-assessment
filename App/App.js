import { StyleSheet, View } from 'react-native';
import React from 'react';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import HomeScreen from './src/HomeScreen';

// Initialize Apollo Client
const client = new ApolloClient({
  uri: 'http://localhost:4010/graphql/',
  cache: new InMemoryCache()
});

export default function App() {
  return(
    <ApolloProvider client={client}>
      <View style={styles.container}>
        <HomeScreen/>
      </View>
    </ApolloProvider>
    );
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
  });
